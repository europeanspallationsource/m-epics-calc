include ${EPICS_ENV_PATH}/module.Makefile

# We do not support swait record since it is deprecated.

TEMPLATES=-none-
SUBSTITUTIONS=-none-
OPIS=-none-

SOURCES += src/main/epics/calcApp/src/transformRecord.c
SOURCES += src/main/epics/calcApp/src/aCalcoutRecord.c
SOURCES += src/main/epics/calcApp/src/sCalcoutRecord.c
SOURCES += src/main/epics/calcApp/src/sseqRecord.c
#SOURCES += src/main/epics/calcApp/src/swaitRecord.c


SOURCES += src/main/epics/calcApp/src/aCalcPerform.c
SOURCES += src/main/epics/calcApp/src/aCalcPostfix.c
#HEADERS += src/main/epics/calcApp/src/aCalcPostfix.h
#HEADERS += src/main/epics/calcApp/src/aCalcPostfixPvt.h
#HEADERS += src/main/epics/calcApp/src/adjustment.h
SOURCES += src/main/epics/calcApp/src/calcUtil.c
SOURCES += src/main/epics/calcApp/src/devaCalcoutSoft.c
SOURCES += src/main/epics/calcApp/src/devsCalcoutSoft.c
#HEADERS += src/main/epics/calcApp/src/myFreeList.h
SOURCES += src/main/epics/calcApp/src/myFreeListLib.c
SOURCES += src/main/epics/calcApp/src/sCalcPerform.c
SOURCES += src/main/epics/calcApp/src/sCalcPostfix.c
#HEADERS += src/main/epics/calcApp/src/sCalcPostfix.h
#HEADERS += src/main/epics/calcApp/src/sCalcPostfixPvt.h
SOURCES += src/main/epics/calcApp/src/subAve.c
#SOURCES += src/main/epics/calcApp/src/test_sCalc.c

SOURCES += src/main/epics/calcApp/src/editSseq.st

SOURCES += src/main/epics/calcApp/src/interp.c
SOURCES += src/main/epics/calcApp/src/arrayTest.c


DBDS += src/main/epics/calcApp/src/transformRecord.dbd
DBDS += src/main/epics/calcApp/src/aCalcoutRecord.dbd
DBDS += src/main/epics/calcApp/src/sCalcoutRecord.dbd
DBDS += src/main/epics/calcApp/src/sseqRecord.dbd
#DBDS += src/main/epics/calcApp/src/swaitRecord.dbd
DBDS += src/main/epics/calcApp/src/calcSupport_LOCAL.dbd
DBDS += src/main/epics/calcApp/src/editSseq.dbd
#DBDS += src/main/epics/calcApp/src/calcSupport_withSSCAN.dbd
#DBDS += src/main/epics/calcApp/src/calcSupport_withSNCSEQ.dbd
